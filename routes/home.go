package routes

import (
	"net/http"

	"github.com/labstack/echo/v4"
	homeController "gitlab.com/Pupan-cpe/go-echo/controllers/home"
)

func InitHomeRoutes(rg *echo.Group) {
	routerGroup := rg

	routerGroup.GET("/",handleRouters)	
	
	routerGroup.GET("/home", handleRouters)	
	routerGroup.GET("/main", homeController.GetAll)	
	routerGroup.GET("/check", homeController.QeryParam)
	routerGroup.GET("/:id", homeController.QeryString)
}

func handleRouters(c echo.Context) error{
	return c.JSON(http.StatusOK, map[string]interface{}{"data":"API VERSION 1.0.0"})
}

