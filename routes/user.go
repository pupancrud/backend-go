package routes

import (
	"github.com/labstack/echo/v4"
	userController "gitlab.com/Pupan-cpe/go-echo/controllers/user"
	middleware "gitlab.com/Pupan-cpe/go-echo/middlewares"
	// middleware "gitlab.com/Pupan-cpe/go-echo/middlewares"
)

func InitUserRoutes(rg *echo.Group) {
	routerGroup := rg.Group("/users")
	routerGroup.POST("/login" , userController.Login)
	routerGroup.POST("/register" , userController.Register)
	
	//q Middleware func
	protected := rg.Group("")
	protected.Use(middleware.AuthJWT())
	protected.GET("/users", userController.GetAll)
	protected.PUT("/users/:id" , userController.Update)
	protected.DELETE("/users/:id" , userController.DeleteById)
	protected.GET("/bb", userController.Profile)
	protected.GET("/token", userController.TokenCheck)

	


	
}

