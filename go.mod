module gitlab.com/Pupan-cpe/go-echo

go 1.16

require (
	github.com/boj/redistore v0.0.0-20180917114910-cd5dcc76aeff
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/joho/godotenv v1.4.0
	github.com/labstack/echo v3.3.10+incompatible // indirect
	github.com/labstack/echo-contrib v0.11.0 // indirect
	github.com/labstack/echo/v4 v4.6.1
	github.com/labstack/gommon v0.3.1 // indirect
	github.com/matthewhartstonge/argon2 v0.1.5
	github.com/pkg/errors v0.9.1 // indirect
	github.com/utahta/echo-sessions v0.4.0 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20211105192438-b53810dc28af // indirect
	golang.org/x/sys v0.0.0-20211107104306-e0b2ad06fe42 // indirect
	gorm.io/driver/mysql v1.1.3
	gorm.io/gorm v1.22.2
)
