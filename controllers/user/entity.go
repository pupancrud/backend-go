package userController

type InputUser struct {
	FullName string `json:"fullname"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type InputLogin struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Response struct {
	Data    interface{} `json:"data"`
	Message string      `json:"message"`
	id      string      `json:"id"`
}
type InputUpdate struct {
	Fullname string `json:"fullname"`
}