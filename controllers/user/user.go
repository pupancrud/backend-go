package userController

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"github.com/matthewhartstonge/argon2"

	configs "gitlab.com/Pupan-cpe/go-echo/config"
	"gitlab.com/Pupan-cpe/go-echo/models"
)


func Profile(c echo.Context) error {

	user :=   c.Get("user")

	return c.JSON(200, map[string]interface{}{
		"data": user,
		"message": "Get API Success",
		})

}

func QeryParam(c echo.Context) error {

	Name := c.QueryParam("name")
	Type := c.QueryParam("type")


	return c.JSON(200, map[string]string{
		"name": Name,
		"type": Type,
	})

}

func QeryString(c echo.Context) error {

	id:= c.Param("id")


	return c.JSON(200, map[string]string{
		"id": id,
	})
}

func Login(c echo.Context) error {
	var input InputLogin
	if err := c.Bind(&input); err != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{"error": err.Error()})
		
	}

	user := models.User{
		Email:    input.Email,
		Password: input.Password,
	}

	//เช็คว่ามีผู้ใช้นี้ในระบบเราหรือไม่
	userAccount := configs.DB.Debug().Where("email = ?", input.Email).First(&user)
	if userAccount.RowsAffected < 1 {
		return	c.JSON(http.StatusNotFound, map[string]string{"error": "ไม่พบผู้ใช้งานนี้ในระบบ"})
		
	}
	ok, _ := argon2.VerifyEncoded([]byte(input.Password), []byte(user.Password))
	if !ok {
		return	c.JSON(http.StatusUnauthorized, map[string]interface{}{"error": "รหัสผ่านไม่ถูกต้อง"})
		
	}else{

//  created Token
token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{

	"user_id": user.ID,
	"host": "this is token Pupan",
	"exp": time.Now().Add(time.Minute*20).Unix(),
	

})

	jwtSecret := os.Getenv("JWT_SECRET")
	accessToken, _ := token.SignedString([]byte(jwtSecret))


		return c.JSON(http.StatusOK, map[string]interface{}{
			"message":"Login Success",
			"token": accessToken,

		})
	}
}

func GetAll(c echo.Context) error {


	var users  []models.User
	configs.DB.Find(&users)


	res := Response{
		Data: users,

		Message: "get api success",
	}
	  

		return c.JSON(200, res)	
}

func Update(c echo.Context)error{


	id := c.Param("id")

	var input InputUpdate

	fmt.Println(input)


	if err := c.Bind(&input); err != nil {
		return c.JSON(http.StatusBadRequest,map[string]string{"error": err.Error()})
		
	}

	var user models.User
	result := configs.DB.First(&user, id)

	if result.RowsAffected < 1 {
		return c.JSON(http.StatusNotFound, map[string]string{"error": "ไม่พบข้อมูลนี้"})
		
	}
	user.FullName = input.Fullname
	resultUpdate := configs.DB.Save(&user)

	if resultUpdate.Error != nil {
		return	 c.JSON(http.StatusBadRequest, map[string]interface{}{"error": resultUpdate.Error})
		
	}

	return	c.JSON(200, map[string]string{
		"message": "Update success",
	})




}


func DeleteById(c echo.Context) error{
	id := c.Param("id")
	var user models.User
	result := configs.DB.First(&user, id)
	if result.RowsAffected < 1 {
		return c.JSON(http.StatusNotFound, map[string]string{"error": "ไม่พบข้อมูลนี้"})

	}

	resultDelete := configs.DB.Delete(&user)

	//db error
	if resultDelete.Error != nil {
		return c.JSON(http.StatusBadRequest, map[string]interface{}{"error": resultDelete.Error}) 
		
	}

	return	c.JSON(200, map[string]string{
			"message": "ลบข้อมูลเรียบร้อย",
		})
}


func TokenCheck(c echo.Context)error{
fmt.Println("loop token")
	return c.JSON(200, true)	

}

func TokenCheck1(c echo.Context)error{
	fmt.Println("loop token")
		return c.JSON(200, true)	
	
	}

func Register(c echo.Context) error{
	
	var input InputUser

	if err := c.Bind(&input); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "Request body are invalid.")
	 }


	user := models.User{

		FullName: input.FullName,
		Email:    input.Email,
		Password: input.Password,
	}
	// check email duplicate

	userExists := configs.DB.Where("email = ?", input.Email).First(&user)

	if userExists.RowsAffected == 1 {

		return c.JSON(http.StatusBadRequest, map[string]string{
			"error": "duplicate Email",
		})
		

	}
	result := configs.DB.Debug().Create(&user)

	// fmt.Println(result)
	if result.Error != nil {
		return	c.JSON(http.StatusBadRequest, result.Error)
		
	} else {
		return	c.JSON(201, map[string]string{

			"message": "Register Success",
		})
	}
}
