package homeController

type InputLogin struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}