package homeController

import (
	"fmt"

	"github.com/labstack/echo/v4"
)


func GetAll(c echo.Context) error {

	return c.String(200, "OK")
}

func QeryParam(c echo.Context) error {

	fmt.Println("Loop")

	Name := c.QueryParam("name")
	Type := c.QueryParam("type")


	return c.JSON(200, map[string]string{
		"name": Name,
		"type": Type,
	})

}

func QeryString(c echo.Context) error {

	id:= c.Param("id")


	return c.JSON(200, map[string]string{
		"id": id,
	})
}



