package middlewares

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	configs "gitlab.com/Pupan-cpe/go-echo/config"
	"gitlab.com/Pupan-cpe/go-echo/models"
)



func AuthJWT() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {

		return func(c echo.Context) error {

			fmt.Println("Start")

			
			auth := c.Request().Header.Get("Authorization")

			// fmt.Println("token:", auth)



			if auth == "" {

				return c.JSON(http.StatusUnauthorized, map[string]interface{}{"error": "Unauthorized"})
			}


			tokenHeader := auth
			accessToken := strings.SplitAfter(tokenHeader, "Bearer")[1] // split  bearer  ออก 
			// fmt.Println(accessToken)
			jwtSecretKey := os.Getenv("JWT_SECRET")


			token, _ := jwt.Parse(strings.Trim(accessToken, " "), func(token *jwt.Token) (interface{}, error) { // jwt.Parse  เอาไว้ check ว่า ใช่ token ของเราจริงไหม
				return []byte(jwtSecretKey), nil
			})
			if !token.Valid {
				return c.JSON(http.StatusUnauthorized,map[string]interface{}{"error": "Unauthorized"})
			} else {
				//q global value result
				claims := token.Claims.(jwt.MapClaims)
				var user models.User
				configs.DB.First(&user, claims["user_id"])
				c.Set("user", user) //? c.Set gobal varibles
				
			}
				//q return to next method if token is exist
				return next(c)
			}
			//do the things

		}

		// return nil
	}