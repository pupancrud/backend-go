package main

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	configs "gitlab.com/Pupan-cpe/go-echo/config"
	"gitlab.com/Pupan-cpe/go-echo/routes"
)




func main() {
    fmt.Println("Welcome to the server")
	godotenv.Load(".env")


	router := SetupRoutes()

	router.Start(":"+os.Getenv("GO_PORT"))
	
	// router.Start(":5000")



	



}

func SetupRoutes()*echo.Echo{

	e:= echo.New()

	
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	  }))  
	  
	e.Debug = true
	configs.ConnectDB()
	
    home := e.Group("/api/v1")
	blnk := e.Group("")
	routes.InitHomeRoutes(blnk)
	routes.InitUserRoutes(home)

	


	return e




}